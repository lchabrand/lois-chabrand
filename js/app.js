/**
 * Created by lois on 07/08/2015.
 */
function scrollTo($element, $speed) {
    $($element).click(function () {
        var page = $(this).attr('href');
        var speed = $speed;
        $('html, body').animate({scrollTop: $(page).offset().top}, speed);
        return false;
    });
}

var lcApp = angular.module('lcApp', ['ngRoute']);

// Configure les routes
lcApp.config(function($routeProvider) {
    $routeProvider
        // route HP
        .when('/', {
            templateUrl : 'index.html',
            controller  : 'mainController'
        })
});

lcApp.controller('mainController', function ($scope) {
    $scope.skills = [
        {'name': 'Nexus S',
            'description': 'Fast just got faster with Nexus S.'},
        {'name': 'Motorola XOOM™ with Wi-Fi',
            'snippet': 'The Next, Next Generation tablet.'},
        {'name': 'MOTOROLA XOOM™',
            'snippet': 'The Next, Next Generation tablet.'}
    ];
});

$(document).ready(function(){
    // quelques modifs du DOM
    scrollTo('.scrollTo', 700);

    $('body').on('click', '.toggle-menu', function() {
        $(this).toggleClass('is-active');
        var $menu = $('#menu');

        if($(this).hasClass('is-active')) {
            $menu.fadeIn();
        } else {
            $menu.fadeOut();
        }
    });

    $('h1').hover(function(){
        $('#bigHr').toggleClass('activeTitle');
    });

    $('.sectionHp').backstretch('css/images/bg1.jpg');

    // on affiche le bouton BTP
    $(window).scroll(function() {
        var height = $(window).scrollTop();
        var viewportHeight = $(window).height();
        var backToTop = $('#backToTop');

        if (height > (viewportHeight - viewportHeight /2)) {
            backToTop.fadeIn();
        } else {
            backToTop.fadeOut();
        }
    });

    // on l'écoute pour remonter
    $("body").on("click touchstart", '#backToTop', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 700);
    });
});